/*
 * IItems.h
 *
 *  Created on: Jul 8, 2013
 *      Author: Tom
 */

#ifndef IITEMS_H_
#define IITEMS_H_

#include <string>
#include <boost/uuid/uuid.hpp>

class IItems {
public:

	virtual void setName(std::string n);
	virtual std::string getName();
	virtual void setType(std::string t);
	virtual std::string getType();
	virtual void setWeight(double w);
	virtual double getWeight();
//	virtual void use() = 0;
	virtual ~IItems();
private:
	std::string sName;
	std::string sType;
	double dWeight;
};

typedef IItems * (__stdcall *CreateItemFn)(void);

#endif /* IITEMS_H_ */
