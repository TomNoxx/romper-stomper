/*
 * IItems.cpp
 *
 *  Created on: Jul 8, 2013
 *      Author: Tom
 */

#include "IItems.h"

void IItems::setName(std::string n) {
	sName = n;
}

std::string IItems::getName() {
	return sName;
}

void IItems::setType(std::string t) {
	sType = t;
}

std::string IItems::getType() {
	return sType;
}

void IItems::setWeight(double w) {
	dWeight = w;
}

double IItems::getWeight() {
	return dWeight;
}

IItems::~IItems() {
	delete this;
}
