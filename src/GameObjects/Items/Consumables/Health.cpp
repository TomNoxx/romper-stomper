/*
 * Health.cpp
 *
 *  Created on: Jul 8, 2013
 *      Author: Tom
 */

#include "Health.h"

Health::Health() {
	//This is just filler until we figure out a better way
	setName("Health");
	setValue(25);
}

int Health::getValue() {
	return iHPValue;
}

void Health::setValue(int hp) {
	iHPValue = hp;
}

IItems * __stdcall Health::Create() {
	Health * hItem = new Health();
	return hItem;
}
//
//void Health::use() {
////	int hp = p->getHealthpoints();
////	hp = hp + this->getValue();
////	p->setHealthpoints(hp);
//}

int Health::quaff() {
	return iHPValue;
}

Health::~Health() {

}

