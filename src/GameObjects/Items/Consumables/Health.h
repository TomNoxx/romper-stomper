/*
 * Health.h
 *
 *  Created on: Jul 8, 2013
 *      Author: Tom
 */

#ifndef HEALTH_H_
#define HEALTH_H_

#include "../IItems.h"
#include "../../../EventHandlers/Controllers/ItemEventController.h"

class Health : public IItems {
private:
	int iHPValue;
public:
	Health();
	int getValue();
	void setValue(int hp);
	static IItems * __stdcall Create();
//	void use();
	int quaff();
	virtual ~Health();
};

#endif /* HEALTH_H_ */
