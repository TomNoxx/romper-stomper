/*
 * MonsterFactory.cpp
 *
 *  Created on: Jun 27, 2013
 *      Author: Tom
 */

#include "MonsterFactory.h"

typedef boost::random_device RNGType;
RNGType rng;

MonsterFactory::MonsterFactory() {
	//TODO see how we can't make this configurable so I don't need to edit code, config file?
	MonsterFactory::registerMonster("Boogalard", &Boogalard::Create);
	MonsterFactory::registerMonster("Troll", &Troll::Create);
}

MonsterFactory::~MonsterFactory() {
	monsterFactoryMap.clear();
}

MonsterFactory * MonsterFactory::getInstance() {
	static MonsterFactory instance;
	return &instance;
}

void MonsterFactory::registerMonster(const std::string &monsterName, CreateMonsterFn createMonster) {
	monsterFactoryMap.insert(std::make_pair<std::string,CreateMonsterFn>(monsterName, createMonster));
}

Monster * MonsterFactory::createMonster(const std::string &monsterName) {
	FactoryMap::iterator itr = monsterFactoryMap.find(monsterName);
	Zone * z = Zone::getInstance();
	boost::random::uniform_int_distribution<> xDist(z->getScreenXLowerBound(), z->getScreenXUpperBound() - 1);
	boost::random::uniform_int_distribution<> yDist(z->getScreenYLowerBound(), z->getScreenYUpperBound() - 1);

	if (itr != monsterFactoryMap.end()) {
		Monster * m = itr->second();
		m->setType(monsterName);
		bool validLoc = false;
		int xLoc;
		int yLoc;
		while (!validLoc) {
			xLoc = xDist(rng);
			yLoc = yDist(rng);
			WorldLocation loc = z->getZone()->at(yLoc).at(xLoc);
			if (loc.isTraversable() && !loc.containsCharacter()) {
				validLoc = true;
			}
		}
		m->setXLoc(xLoc);
		m->setYLoc(yLoc);
		MonsterList::getInstance()->insertMonster(m->getUUID(), m);
		Zone::getInstance()->getZone()->at(yLoc).at(xLoc).setCharacter(m);
		MonsterEventController::getInstance()->addConnection(m);
		//TODO create a monster thread (may do this in the monster spawner, or do all of this in a singe separate thread)
		//the original threading for each monster was merely proof of concept
		return m;
	}
	return NULL;
}

