/*
a * ConsumableFactory.cpp
 *
 *  Created on: Jul 8, 2013
 *      Author: Tom
 */

#include "ItemFactory.h"

ItemFactory::ItemFactory() {
	ItemFactory::registerItem("Health", &Health::Create);
}

ItemFactory::~ItemFactory() {
	mItemFactoryMap.clear();
}

ItemFactory * ItemFactory::getInstance() {
	static ItemFactory instance;
	return &instance;
}

void ItemFactory::registerItem(const std::string &itemName, CreateItemFn createItem) {
	mItemFactoryMap.insert(std::make_pair<std::string,CreateItemFn>(itemName, createItem));
}

IItems * ItemFactory::createItem(const std::string &itemName) {
	FactoryMap::iterator itr = mItemFactoryMap.find(itemName);
	if (itr != mItemFactoryMap.end()) {
		return itr->second();
	}
	return NULL;
}
