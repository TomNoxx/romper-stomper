/*
 * MonsterFactory.h
 *
 *  Created on: Jun 27, 2013
 *      Author: Tom
 */

//Completed with the help of CodeProject.com
//http://www.codeproject.com/Articles/363338/Factory-Pattern-in-Cplusplus

#ifndef MONSTERFACTORY_H_
#define MONSTERFACTORY_H_

#include <stddef.h>
#include <string>
#include <map>
#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include "../Character/Monster/Monster.h"
#include "../Character/Monster/MonsterClasses/Boogalard.h"
#include "../Character/Monster/MonsterClasses/Troll.h"
#include "../Singletons/Zone.h"
#include "../Singletons/MonsterSingleton.h"
#include "../EventHandlers/Controllers/MonsterEventController.h"

class MonsterFactory {
private:
	MonsterFactory();
	MonsterFactory(const MonsterFactory &) { }
	MonsterFactory &operator=(const MonsterFactory &) { return *this; }
	typedef std::map<std::string, CreateMonsterFn> FactoryMap;
	FactoryMap monsterFactoryMap;
public:
	static MonsterFactory * getInstance();
	virtual ~MonsterFactory();

	void registerMonster(const std::string &monsterName, CreateMonsterFn createMonster);
	Monster * createMonster(const std::string &monsterName);
};

#endif /* MONSTERFACTORY_H_ */
