/*
 * ConsumableFactory.h
 *
 *  Created on: Jul 8, 2013
 *      Author: Tom
 */

#ifndef ITEMFACTORY_H_
#define ITEMFACTORY_H_

#include <stddef.h>
#include <map>
#include <string>
#include "../GameObjects/Items/IItems.h"
#include "../GameObjects/Items/Consumables/Health.h"


class ItemFactory {
private:
	ItemFactory();
	ItemFactory(const ItemFactory &) { }
	ItemFactory &operator=(const ItemFactory &) { return *this; }
	typedef std::map<std::string, CreateItemFn> FactoryMap;
	FactoryMap mItemFactoryMap;
public:
	static ItemFactory * getInstance();
	virtual ~ItemFactory();

	void registerItem(const std::string &itemName, CreateItemFn createItem);
	IItems * createItem(const std::string &itemName);
};

#endif /* ITEMFACTORY_H_ */
