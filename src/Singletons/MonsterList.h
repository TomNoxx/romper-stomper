/*
 * MonsterList.h
 *
 *  Created on: Jul 5, 2013
 *      Author: Tom
 */

#ifndef MONSTERLIST_H_
#define MONSTERLIST_H_

#include <stddef.h>
#include <cstdlib>
#include <map>
#include <boost/uuid/uuid.hpp>
#include <boost/thread.hpp>
#include "../Character/Monster/Monster.h"

class MonsterList {
private:
	std::map<boost::uuids::uuid, Monster *> monsterMap;
	std::map<std::string, SDL_Surface *> monsterSurfaceMap;
	static MonsterList * instance;
	static bool instanceFlag;
	boost::uuids::uuid uuid;
	MonsterList();
public:
	static MonsterList * getInstance();
	std::map<boost::uuids::uuid, Monster *> * getMonsterMap();
	void insertMonster(boost::uuids::uuid uuid, Monster * m);
	Monster * getMonster(boost::uuids::uuid uuid);
	void removeMonster(boost::uuids::uuid uuid);
	virtual ~MonsterList();
};

#endif /* MONSTERLIST_H_ */
