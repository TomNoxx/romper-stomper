/*
 * MonsterUtilities.h
 *
 *  Created on: Jun 27, 2013
 *      Author: Tom
 */

#ifndef MONSTERSINGLETON_H_
#define MONSTERSINGLETON_H_

#include <stddef.h>
#include <cstdlib>
#include <boost/random.hpp>
#include <boost/random/random_device.hpp>
#include "../Character/Monster/Monster.h"
#include "../Utilities/CharacterUtilities.h"

class MonsterSingleton {
private:
	static MonsterSingleton * instance;
	static bool instanceFlag;
	//TODO don't think i need this anymore
	static bool timeToQuit;
	MonsterSingleton();
public:
	static MonsterSingleton * getInstance();
	void setQuit(bool q);
	bool isQuit();
	void move(Monster * m);
	~MonsterSingleton();

};

#endif /* MONSTERSingleton_H_ */
