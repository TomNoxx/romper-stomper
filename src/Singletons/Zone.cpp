/*
 * Zone.cpp
 *
 *  Created on: Jun 20, 2013
 *      Author: Tom
 */

#include "Zone.h"

bool Zone::instanceFlag = false;
Zone * Zone::instance = NULL;

Zone::Zone() {

}

Zone * Zone::getInstance() {
	if (instanceFlag) {
		return instance;
	} else {
		instance = new Zone();
		instanceFlag = true;
		return instance;
	}
}

std::vector<std::vector<WorldLocation> > * Zone::getZone() {
	return &zone;
}

void Zone::setZone(std::vector<std::vector<WorldLocation> > zone) {
	this->zone = zone;
}

int Zone::getScreenHeight() {
	return screen_resolution.height;
}

void Zone::setScreenHeight(int h) {
	screen_resolution.height = h;
}

int Zone::getScreenWidth() {
	return screen_resolution.width;
}

void Zone::setScreenWidth(int w) {
	screen_resolution.width = w;
}

int Zone::getScreenBpp() {
	return screen_resolution.bpp;
}

void Zone::setScreenBpp(int b) {
	screen_resolution.bpp = b;
}

int Zone::getScreenXLowerBound() const {
	return screenXLowerBound;
}

void Zone::setScreenXLowerBound(int xLB) {
	screenXLowerBound = xLB;
}

int Zone::getScreenXUpperBound() const {
	return screenXUpperBound;
}

void Zone::setScreenXUpperBound(int xUB) {
	screenXUpperBound = xUB;
}

int Zone::getScreenYLowerBound() const {
	return screenYLowerBound;
}

void Zone::setScreenYLowerBound(int yLB) {
	screenYLowerBound = yLB;
}

int Zone::getScreenYUpperBound() const {
	return screenYUpperBound;
}

void Zone::setScreenYUpperBound(int yUB) {
	screenYUpperBound = yUB;
}

SDL_Surface * Zone::getImageFromImageMap(std::string name) {
	std::map<std::string, SDL_Surface *>::iterator itr = imageMap.find(name);
	return itr->second;
}

void Zone::insertImageIntoImageMap(std::string name, SDL_Surface * surface) {
	imageMap.insert(std::make_pair<std::string,SDL_Surface *>(name, surface));
}

std::map<std::string, SDL_Surface *> * Zone::getImageMap() {
	return &imageMap;
}

Zone::~Zone() {
	delete instance;
}

