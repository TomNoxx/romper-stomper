/*
 * MonsterList.cpp
 *
 *  Created on: Jul 5, 2013
 *      Author: Tom
 */

#include "MonsterList.h"

MonsterList * MonsterList::instance = NULL;
bool MonsterList::instanceFlag = false;

MonsterList::MonsterList() {

}

MonsterList * MonsterList::getInstance() {
	if (instanceFlag) {
		return instance;
	} else {
		instance = new MonsterList();
		instanceFlag = true;
		return instance;
	}
}

std::map<boost::uuids::uuid, Monster *> * MonsterList::getMonsterMap() {
	return &monsterMap;
}

void MonsterList::insertMonster(boost::uuids::uuid uuid, Monster * m) {
	monsterMap.insert(std::make_pair<boost::uuids::uuid, Monster *>(uuid,m));
}

Monster * MonsterList::getMonster(boost::uuids::uuid uuid) {
	return monsterMap.find(uuid)->second;
}

void MonsterList::removeMonster(boost::uuids::uuid uuid) {
	monsterMap.erase(uuid);
}

MonsterList::~MonsterList() {
	delete instance;
	monsterMap.clear();
}

