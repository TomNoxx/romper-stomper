/*
 * MonsterUtilities.cpp
 *
 *  Created on: Jun 27, 2013
 *      Author: Tom
 */

#include "MonsterSingleton.h"

bool MonsterSingleton::instanceFlag = false;
bool MonsterSingleton::timeToQuit = false;
MonsterSingleton * MonsterSingleton::instance = NULL;
typedef boost::random_device RNGType;
RNGType moveRNG;

MonsterSingleton::MonsterSingleton() {
	timeToQuit = false;
}

MonsterSingleton * MonsterSingleton::getInstance() {
	if (instanceFlag) {
		return instance;
	} else {
		instance = new MonsterSingleton();
		instanceFlag = true;
		return instance;
	}
}

void MonsterSingleton::move(Monster * m) {
	//Probably not the idea random choice, but it's simple and the <random> library was proving
	//annoying as an "experimental" item.  Even after changing the compiler invocation arguments
	//it was still proving troublesome
	boost::random::uniform_int_distribution<> dist(0, 3);
	switch (dist(moveRNG)) {
	case 0:
		CharacterUtilities::moveUp(m);
		break;
	case 1:
		CharacterUtilities::moveLeft(m);
		break;
	case 2:
		CharacterUtilities::moveDown(m);
		break;
	case 3:
		CharacterUtilities::moveRight(m);
		break;
	default:
		CharacterUtilities::moveUp(m);
	}

	//TODO use sleep in GeneralUtilities to move
	//TODO randomly pick one of 4 directions.
	//TODO switch clause to pick the right move function
}

void MonsterSingleton::setQuit(bool q) {
	timeToQuit = q;
}

bool MonsterSingleton::isQuit() {
	return timeToQuit;
}

MonsterSingleton::~MonsterSingleton() {
	delete instance;
}
