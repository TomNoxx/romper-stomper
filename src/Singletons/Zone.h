/*
 * Zone.h
 *
 *  Created on: Jun 20, 2013
 *      Author: Tom
 */

#ifndef ZONE_H_
#define ZONE_H_

#include <vector>
#include <map>
#include <string>
#include <stddef.h>
#include "SDL.h"
#include "../World/WorldLocation.h"


//This is a singleton because we only need one zone
class Zone {
private:
	//height and width should always be a multiple of 50, for now
	struct RESOLUTION {
		int height;
		int width;
		int bpp;
	} screen_resolution = { 500, 500, 32 };
	static Zone * instance;
	static bool instanceFlag;
	std::vector<std::vector<WorldLocation> > zone;
	std::map<std::string, SDL_Surface *> imageMap;
	int screenYLowerBound, screenYUpperBound;
	int screenXLowerBound, screenXUpperBound;
	Zone();
public:
	static Zone * getInstance();
	std::vector<std::vector<WorldLocation> > * getZone();
	void setZone(std::vector<std::vector<WorldLocation> > zone);
	int getScreenHeight();
	void setScreenHeight(int h);
	int getScreenWidth();
	void setScreenWidth(int w);
	int getScreenBpp();
	void setScreenBpp(int b);
	int getScreenXLowerBound() const;
	void setScreenXLowerBound(int xLB);
	int getScreenXUpperBound() const;
	void setScreenXUpperBound(int xUB);
	int getScreenYLowerBound() const;
	void setScreenYLowerBound(int yLB);
	int getScreenYUpperBound() const;
	void setScreenYUpperBound(int yUB);
	SDL_Surface * getImageFromImageMap(std::string name);
	void insertImageIntoImageMap(std::string name, SDL_Surface * surface);
	std::map<std::string, SDL_Surface *> * getImageMap();
	virtual ~Zone();
};

#endif /* ZONE_H_ */
