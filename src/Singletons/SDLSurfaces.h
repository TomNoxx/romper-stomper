/*
 * SDLSurfaces.h
 *
 *  Created on: Jul 7, 2013
 *      Author: Tom
 */

#ifndef SDLSURFACES_H_
#define SDLSURFACES_H_

#include <string>
#include <map>
#include <stddef.h>
#include "SDL.h"


class SDLSurfaces {
private:
	static SDLSurfaces * instance;
	static bool instanceFlag;
	std::map<std::string, SDL_Surface *> characterImageMap;
	std::map<std::string, SDL_Surface *> zoneImageMap;
	SDLSurfaces();
public:
	static SDLSurfaces * getInstance();
	SDL_Surface * getFromZoneImageMap(std::string name);
	void insertIntoZoneImageMap(std::string name, SDL_Surface * surface);
	std::map<std::string, SDL_Surface *> * getZoneImageMap();
	SDL_Surface * getFromCharacterImageMap(std::string name);
	void insertIntoCharacterImageMap(std::string name, SDL_Surface * surface);
	std::map<std::string, SDL_Surface *> * getCharacterImageMap();
	virtual ~SDLSurfaces();
};

#endif /* SDLSURFACES_H_ */
