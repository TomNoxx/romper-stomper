/*
 * SDLSurfaces.cpp
 *
 *  Created on: Jul 7, 2013
 *      Author: Tom
 */

#include "SDLSurfaces.h"

SDLSurfaces * SDLSurfaces::instance = NULL;
bool SDLSurfaces::instanceFlag = false;

SDLSurfaces::SDLSurfaces() {
	// TODO Auto-generated constructor stub

}

SDLSurfaces * SDLSurfaces::getInstance() {
	if (instanceFlag) {
		return instance;
	} else {
		instance = new SDLSurfaces();
		instanceFlag = true;
		return instance;
	}
}

SDL_Surface * SDLSurfaces::getFromZoneImageMap(std::string name) {
	std::map<std::string, SDL_Surface *>::iterator itr = zoneImageMap.find(name);
	return itr->second;
}

void SDLSurfaces::insertIntoZoneImageMap(std::string name, SDL_Surface * surface) {
	zoneImageMap.insert(std::make_pair<std::string,SDL_Surface *>(name, surface));
}

std::map<std::string, SDL_Surface *> * SDLSurfaces::getZoneImageMap() {
	return &zoneImageMap;
}

SDL_Surface * SDLSurfaces::getFromCharacterImageMap(std::string name) {
	std::map<std::string, SDL_Surface *>::iterator itr = characterImageMap.find(name);
	return itr->second;
}

void SDLSurfaces::insertIntoCharacterImageMap(std::string name, SDL_Surface * surface) {
	characterImageMap.insert(std::make_pair<std::string,SDL_Surface *>(name, surface));
}

std::map<std::string, SDL_Surface *> * SDLSurfaces::getCharacterImageMap() {
	return &characterImageMap;
}


SDLSurfaces::~SDLSurfaces() {
	// TODO Auto-generated destructor stub
}

