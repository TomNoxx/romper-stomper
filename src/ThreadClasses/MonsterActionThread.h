/*
 * MonsterUtilities.h
 *
 *  Created on: Jul 2, 2013
 *      Author: Tom
 */

#ifndef MONSTERACTIONTHREAD_H_
#define MONSTERACTIONTHREAD_H_

#include "../Character/Monster/Monster.h"
#include "../Singletons/MonsterSingleton.h"
#include "../Singletons/MonsterList.h"
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/progress.hpp>

//This is my thread functor
class MonsterActionThread {
private:
	Monster * monster;
//	void moveMonster();
public:
	MonsterActionThread(Monster * m);
	void operator()();
	virtual ~MonsterActionThread();
};

#endif /* MONSTERACTIONTHREAD_H_ */
