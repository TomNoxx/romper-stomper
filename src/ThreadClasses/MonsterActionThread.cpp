/*
 * MonsterUtilities.cpp
 *
 *  Created on: Jul 2, 2013
 *      Author: Tom
 */

#include "MonsterActionThread.h"

MonsterActionThread::MonsterActionThread(Monster * m) {
	monster = m;
}

void MonsterActionThread::operator()() {
	MonsterSingleton * ms = MonsterSingleton::getInstance();
	boost::progress_timer timer;
	while (!ms->isQuit()) {
		//TODO create a child thread here so that the health point check can still happen continuously
		if (monster->getHealthpoints() <= 0) {
			break;
		}
		double elapsedTime = timer.elapsed();
//		GeneralUtilities::sleep(2);
		if (elapsedTime > 2) {
			ms->move(monster);
			timer.restart();
		}
	}
}


MonsterActionThread::~MonsterActionThread() {
	// TODO Auto-generated destructor stub
}

