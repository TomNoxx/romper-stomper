/*
 * WorldUtilities.cpp
 *
 *  Created on: yun 6, 2013
 *      Author: Tom
 */

#include "WorldUtilities.h"

//parse the dimensions from a zone file.
//This is a line that looks like such:
//  20x10 (width)x(height)
void WorldUtilities::parseDimensions(std::string firstLine, int & width, int & height) {
	std::vector<std::string> dimensions = GeneralUtilities::split(firstLine, 'x');
	width = atoi(dimensions[0].c_str());
	height = atoi(dimensions[1].c_str());
}

//Split a zone row into a vector and assign that to the row array via rowArrayPtr
void WorldUtilities::parseZoneRow(std::string row, std::vector<std::string> rowArray, int size) {
	std::vector<std::string> rowVector = GeneralUtilities::split(row, ':');
	for (int i = 0; i < size; i++) {
		rowArray.at(i) = rowVector.at(i);
	}
}

//This function builds the row for the zone valueArray by getting the string values of each
//tile type and setting that WorldLocation and pushing that WorldLocation into the valueArray
void WorldUtilities::buildZoneValueRow(std::vector<std::string>  * keyVector, std::vector<WorldLocation> & valueVector, int size, std::map<std::string, std::string> & tileMapping) {
	for (int i = 0; i < size; i++) {
		std::map<std::string, std::string>::iterator itr;
		itr = tileMapping.find(keyVector->at(i));
		if (itr != tileMapping.end()) {
			WorldLocation loc = WorldLocation();
			try {
				std::string tileValue = itr->second.c_str();
				//TODO figure out a more extensible/configurable way of doing this
				if ("Mountain" == tileValue || "Blank" == tileValue) {
					loc.setTraversable(false);
				}
				loc.setTileValue(tileValue);
			} catch (...) {}
			valueVector.push_back(loc);
		}
	}
}

//invert the 2d vector before printing it to the screen.
std::vector<std::vector<WorldLocation> > WorldUtilities::invertWorldForPrinting() {
	std::vector<WorldLocation> rowForInvertedZone;
	std::vector<std::vector<WorldLocation> > invertedZone;
	std::vector<std::vector<WorldLocation> > * zoneToDraw = Zone::getInstance()->getZone();
	for (int x = 0; x < (int) zoneToDraw->size(); x++) {
		for (int y = 0; y < (int) zoneToDraw->at(x).size(); y++) {
			rowForInvertedZone.push_back(zoneToDraw->at(y).at(x));
		}
		invertedZone.push_back(rowForInvertedZone);
		rowForInvertedZone.clear();
	}
	return invertedZone;
}

//parse through the tile mapping file and build a map of key value pairs
//This is used to assign WorldLocations the correct string value for its tileValue
void WorldUtilities::setTileValueMapping(std::map<std::string, std::string> & tileMapping, std::string mappingFile) {
	std::string line;
	std::ifstream zoneMappingStream (mappingFile.c_str(), std::ifstream::in);
	if(zoneMappingStream.is_open()) {
		while(!zoneMappingStream.eof()) {
			zoneMappingStream >> line;
			std::string key;
			std::string value;
			GeneralUtilities::parseProperties(line, key, value);
			tileMapping.insert(std::make_pair<std::string,std::string>(key,value));
		}
		zoneMappingStream.close();
	}
}

//This takes the zone file and builds a 2 dimensional vector of WOrldLocations that is the zone.
//it populates the tileValue for those WorldLocations with the value tiles will need to obtain their image
void WorldUtilities::setZoneValuesToWorld(std::map<std::string, std::string> & tileMapping, std::string zoneFile) {
	std::vector<std::vector<WorldLocation> > myWorld;
	std::string line;
	std::ifstream zoneStream (zoneFile.c_str(), std::ifstream::in);
	Zone * z = Zone::getInstance();
	int w = z->getScreenWidth() / 50;
//	int h = z->getScreenHeight() / 50;
	if(zoneStream.is_open()) {
		//the first line is the zone dimensions
//		zoneStream >> line;
//		parseDimensions(line, w, h);
		int rowCounter = 0;
		//every subsequent line is a part of the map
		while(!zoneStream.eof()) {
//			std::string keyRow [w];
			std::vector<WorldLocation> valueRow;
			//TODO should try/catch here
			zoneStream >> line;
			std::vector<std::string> keyRow = GeneralUtilities::split(line, ':');
//			parseZoneRow(line, keyRow, w);
			//TODO refer to old code to insert into myWorld here
			buildZoneValueRow(&keyRow, valueRow, w, tileMapping);
			myWorld.push_back(valueRow);
			++rowCounter;
			//TODO end try/catch
			keyRow.clear();
		}

	}
	z->setZone(myWorld);
}

//hopefully this loads each image to a single map, that way when we set surfaces we aren't loading each image over and over
bool WorldUtilities::loadZoneImages(std::map<std::string, std::string> * tileMapping) {
	Zone * z = Zone::getInstance();
	std::map<std::string, std::string>::iterator itr = tileMapping->begin();
	while (itr != tileMapping->end()) {
		std::string tileValue = "Images/World/" + itr->second + ".bmp";
		z->insertImageIntoImageMap(itr->second, SDLUtilities::loadImage(tileValue));
		++itr;
	}
	return true;
}

//Load Zone Images based on tileValue
bool WorldUtilities::setZoneImages() {
	Zone * z = Zone::getInstance();
	std::vector<std::vector<WorldLocation> > * zone = z->getZone();
	for (int y = 0; y < (int) zone->size(); y++) {
		for (int x = 0; x < (int) zone->at(y).size(); x++) {
			//Set the image pointer the zone singleton zone map points to from the image map in the zone singleton
			zone->at(y).at(x).setSurface(z->getImageFromImageMap(zone->at(y).at(x).getTileValue()));
		}
	}
	return true;
}

//draw the zoneVector to the screen
bool WorldUtilities::drawZone(SDL_Surface * screen) {
	Zone * z = Zone::getInstance();
//	std::vector<std::vector<WorldLocation> > * zone = z->getZone();
	for (int y = z->getScreenYLowerBound(); y < z->getScreenYUpperBound(); y++) {
		for (int x = z->getScreenXLowerBound(); x < z->getScreenXUpperBound(); x++) {
			std::vector<std::vector<WorldLocation> > invertedZone = invertWorldForPrinting();
			if (invertedZone.at(y).at(x).containsCharacter()) {
				SDLUtilities::apply_surface((y * 50), (x * 50), invertedZone.at(y).at(x).getCharacter()->getCharacterSurface(), screen);
			} else if (invertedZone.at(y).at(x).containsItems()) {
				//TODO draw items
				SDLUtilities::apply_surface((y * 50), (x * 50), SDLUtilities::loadImage("error.bmp"), screen);
			} else {
				SDLUtilities::apply_surface((y * 50), (x * 50), invertedZone.at(y).at(x).getSurface(), screen);
			}
		}
	}
	return true;
}

//Free each SDL_Surface in zoneVector from memory
bool WorldUtilities::freeZoneSurfaces() {
	Zone * z = Zone::getInstance();
	std::map<std::string, SDL_Surface *> * zoneSurfaceMap = z->getImageMap();
	std::map<std::string, SDL_Surface *>::iterator itr = zoneSurfaceMap->begin();
	while (itr != zoneSurfaceMap->end()) {
		SDLUtilities::freeSurface(itr->second);
		++itr;
	}
	zoneSurfaceMap->clear();
	//clean up the zone vector
	z->getZone()->clear();
	return true;
}


