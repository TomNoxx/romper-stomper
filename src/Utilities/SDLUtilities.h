/*
 * SDLUtilities.h
 *
 *  Created on: Jun 5, 2013
 *      Author: Tom
 */

#ifndef SDLUTILITIES_H_
#define SDLUTILITIES_H_

#include "SDL.h"
#include "SDL_ttf.h"
#include "../World/WorldLocation.h"
#include <string>
#include <vector>

class SDLUtilities {
public:
	static void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip = NULL );

	static bool fillBackground(SDL_Surface *screen);

	static SDL_Surface *loadImage(std::string file);

	static void freeSurface(SDL_Surface * surface);

	static SDL_Surface *renderExistingDynamicText(SDL_Surface * oldSurface, TTF_Font * font, std::string message, SDL_Color * color);

//	static void drawWorld(std::vector<std::vector<WorldLocation> > & theZone, int * startingX, int * startingY);

};

#endif /* SDLUTILITIES_H_ */
