/*
 * CharacterUtilities.cpp
 *
 *  Created on: Jun 20, 2013
 *      Author: Tom
 */

#include "CharacterUtilities.h"

/**** Movement Notes ****/
// Without a sense of direction or seperate hotkeys for directional attack,
//binding attacking with movement has created some interesting dilemmas.
//TODO figure out how to manage health and death outside of movement if necessary

void CharacterUtilities::removeCharacterForMovement(Character * c) {
        Zone * z = Zone::getInstance();
        z->getZone()->at(c->getYLoc()).at(c->getXLoc()).removeCharacter();
}

void CharacterUtilities::addCharacterForMovement(Character * c) {
        Zone * z = Zone::getInstance();
        z->getZone()->at(c->getYLoc()).at(c->getXLoc()).setCharacter(c);
}

//TODO several todos here
void CharacterUtilities::manageCombat(Character* c, WorldLocation* newLoc) {
        //TODO Attack
        Character* defender = newLoc->getCharacter();
        CombatUtilities::attack(c, defender);
        if (defender->getHealthpoints() <= 0) {
                if (defender->getType() == "Player") {
//              if (defender->getType().compare("Player") == 0) {
                        //TODO game over scenario
                                //this could be a custom event that is being listened for as a event test
                        //game over scenario for player
                } else {
					//TODO drop health item
                	MonsterList * ml = MonsterList::getInstance();
                	Monster * m = ml->getMonster(defender->getUUID());
                	if (m != NULL) {
                		m->dropItem("Health");
                	}
					//drop health item
					//remove monster from list
					ml->removeMonster(defender->getUUID());
                }
                defender->freeCharacterSurface();
                Zone::getInstance()->getZone()->at(defender->getYLoc()).at(defender->getXLoc()).removeCharacter();
        }
        //TODO restore health mechanic would be between them if a player kills a monster
        //TODO the other way around since attackers take damage too
        if (c->getHealthpoints() <= 0) {
//                if (c->getType() == "Player") {
                if (c->getType().compare("Player") == 0) {
                        //TODO game over scenario
                } else {
                        MonsterList::getInstance()->removeMonster(c->getUUID());
                }
                c->freeCharacterSurface();
                Zone::getInstance()->getZone()->at(defender->getYLoc()).at(defender->getXLoc()).removeCharacter();
        }

}

void CharacterUtilities::moveToNewYLoc(int newY, Character* c) {
        removeCharacterForMovement(c);
        c->setYLoc(newY);
        addCharacterForMovement(c);
}

void CharacterUtilities::moveToNewXLoc(int newX, Character* c) {
        removeCharacterForMovement(c);
        c->setXLoc(newX);
        addCharacterForMovement(c);
}

bool CharacterUtilities::moveUp(Character * c) {
        Zone * z = Zone::getInstance();
        int y = c->getYLoc();
        int newY = y - 1;
        if (y > 0) {
                WorldLocation * newLoc = &z->getZone()->at(newY).at(c->getXLoc());
                if (newLoc->containsCharacter()) {
                        manageCombat(c, newLoc);
                        return true;
                } else if (newLoc->isTraversable()) {
                        moveToNewYLoc(newY, c);
                        return true;
                }
        }
        return false;
}

bool CharacterUtilities::moveDown(Character * c) {
        Zone * z = Zone::getInstance();
        int y = c->getYLoc();
        int newY = y + 1;
        if (y < (int) z->getZone()->size() - 1) {
                WorldLocation * newLoc = &z->getZone()->at(newY).at(c->getXLoc());
                if (newLoc->containsCharacter()) {
                        manageCombat(c, newLoc);
                        return true;
                } else if (newLoc->isTraversable()) {
                        moveToNewYLoc(newY, c);
                        return true;
                }
        }
        return false;
}

bool CharacterUtilities::moveLeft(Character * c) {
        Zone * z = Zone::getInstance();
        int x = c->getXLoc();
        int newX = x - 1;
        if (x > 0) {
                WorldLocation * newLoc = &z->getZone()->at(c->getYLoc()).at(newX);
                if (newLoc->containsCharacter()) {
                        manageCombat(c, newLoc);
                        return true;
                } else if (newLoc->isTraversable()) {
                        moveToNewXLoc(newX, c);
                        return true;
                }
        }
        return false;
}

bool CharacterUtilities::moveRight(Character * c) {
        Zone * z = Zone::getInstance();
        int x = c->getXLoc();
        int newX = x + 1;
        if (x < (int) z->getZone()->at(c->getYLoc()).size() - 1) {
                WorldLocation * newLoc = &z->getZone()->at(c->getYLoc()).at(newX);
                if (newLoc->containsCharacter()) {
                        manageCombat(c, newLoc);
                        return true;
                } else if (newLoc->isTraversable()) {
                        moveToNewXLoc(newX, c);
                        return true;
                }
        }
        return false;
}

void CharacterUtilities::buildCharacterMapping(std::string mappingFile) {
        std::string line;
        std::ifstream zoneMappingStream (mappingFile.c_str(), std::ifstream::in);
        if(zoneMappingStream.is_open()) {
                while(!zoneMappingStream.eof()) {
                        zoneMappingStream >> line;
                        std::string imageName;
                        std::string imagePath;
                        GeneralUtilities::parseProperties(line, imageName, imagePath);
                        std::string surfaceImage = imagePath + imageName + ".bmp";
                        SDL_Surface * s = SDLUtilities::loadImage(surfaceImage);
                        SDLSurfaces::getInstance()->insertIntoCharacterImageMap(imageName, s);
                }
                zoneMappingStream.close();
        }
}
