/*
 * GeneralUtilities.cpp
 *
 *  Created on: Jun 5, 2013
 *      Author: Tom
 */

#include "GeneralUtilities.h"

std::string GeneralUtilities::convertInt(int number)
{
	std::stringstream ss;//create a stringstream
	ss << number;//add number to the stream
	return ss.str();//return a string with the contents of the stream
}

std::string GeneralUtilities::convertDouble(double number) {
	std::stringstream ss;
	ss << number;
	return ss.str();
}

void GeneralUtilities::sleep(clock_t sec) // clock_t is a like typedef unsigned int clock_t. Use clock_t instead of integer in this context
{
	clock_t start_time = clock();
	clock_t end_time = sec * 1000 + start_time;
	while(clock() != end_time);
}

//parse the key value pair from a file where each line looks as such
//key=value
//key=value
//etc
void GeneralUtilities::parseProperties(std::string keyValuePair, std::string & key, std::string & value) {
	std::vector<std::string> keyValuePairVector = split(keyValuePair, '=');
	key = keyValuePairVector[0];
	value = keyValuePairVector[1];
}

//TODO look at these two.  I feel like one is not necessary
void GeneralUtilities::split(const std::string & s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
}

std::vector<std::string>  GeneralUtilities::split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}


//#if defined(__WIN32__) || defined(_WIN32) || defined(WIN32) || defined(__WINDOWS__) || defined(__TOS_WIN__)
//
//  void GeneralUtilities::delay( unsigned long ms )
//    {
//    Sleep( ms );
//    }
//
//#else  /* presume POSIX */
//
//  void GeneralUtilities::delay( unsigned long ms )
//    {
//    usleep( ms * 1000 );
//    }
//
//#endif

