/*
 * CombatUtilities.cpp
 *
 *  Created on: Jun 26, 2013
 *      Author: Tom
 */

#include "CombatUtilities.h"

void CombatUtilities::attack(Character * attacker, Character * defender) {
	int attackerDamage = attacker->getAttackBase();
	int attackerHealth = attacker->getHealthpoints();
	int defenderDamage = defender->getAttackBase();
	int defenderHealth = defender->getHealthpoints();
	//Just a simple combat system for proof of concept purposes
	defender->setHealthpoints((defenderHealth - attackerDamage));
	attacker->setHealthpoints((attackerHealth - (defenderDamage / 2)));
}

