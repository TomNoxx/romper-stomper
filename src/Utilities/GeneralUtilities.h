/*
 * GeneralUtilities.h
 *
 *  Created on: Jun 5, 2013
 *      Author: Tom
 */

#ifndef GENERALUTILITIES_H_
#define GENERALUTILITIES_H_

#include <string>
#include <sstream>
#include <ctime>
#include <vector>
//#include <windows.h>
//#include <unistd.h>

class GeneralUtilities {
private:
public:
	static std::string convertInt(int number);
	static std::string convertDouble(double number);
	static void sleep(clock_t sec);
	static void parseProperties(std::string keyValuePair, std::string & key, std::string & value);
	static void split(const std::string &s, char delim, std::vector<std::string> &elems);
	static std::vector<std::string> split(const std::string &s, char delim);
};

#endif /* GENERALUTILITIES_H_ */
