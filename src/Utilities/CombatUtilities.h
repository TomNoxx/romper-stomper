/*
 * CombatUtilities.h
 *
 *  Created on: Jun 26, 2013
 *      Author: Tom
 */

#ifndef COMBATUTILITIES_H_
#define COMBATUTILITIES_H_

#include "../Character/Character.h"
#include "../Character/Player/Player.h"
#include "../Character/Monster/Monster.h"

class CombatUtilities {
public:
	static void attack(Character * attacker, Character * Defender);
};

#endif /* COMBATUTILITIES_H_ */
