/*
 * WorldUtilities.h
 *
 *  Created on: Jun 6, 2013
 *      Author: Tom
 */

#ifndef WORLDUTILITIES_H_
#define WORLDUTILITIES_H_

#include <map>
#include <string>
#include <fstream>
#include <vector>
#include <sstream>
#include <iostream>
#include "../World/WorldLocation.h"
#include "../Singletons/Zone.h"
#include "GeneralUtilities.h"
#include "SDL/SDL.h"

class WorldUtilities {
private:
	static void parseDimensions(std::string firstLine, int &width, int &height);
	static void parseZoneRow(std::string row, std::vector<std::string> rowArray, int size);
	static void buildZoneValueRow(std::vector<std::string> *keyArrayPtr, std::vector<WorldLocation> & valueArray, int size, std::map<std::string, std::string> & tileMapping);
	static std::vector<std::vector<WorldLocation> > invertWorldForPrinting();
public:
	static void setTileValueMapping(std::map<std::string, std::string> &tileMapping, std::string mappingFile);
	static void setZoneValuesToWorld(std::map<std::string, std::string> & tileMapping, std::string zoneFile);
	static bool loadZoneImages(std::map<std::string, std::string> * tileMapping);
	static bool setZoneImages();
	static bool drawZone(SDL_Surface * screen);
	static bool freeZoneSurfaces();
};

#endif /* WORLDUTILITIES_H_ */
