/*
 * SDLUtilities.cpp
 *
 *  Created on: Jun 5, 2013
 *      Author: Tom
 */

#include "SDLUtilities.h"

void SDLUtilities::apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination, SDL_Rect* clip ) {
	//Temporary rectangle to hold the offsets
	SDL_Rect offset;

	//Get the offsets
	offset.x = x;
	offset.y = y;

	//Blit the surface
	SDL_BlitSurface( source, clip, destination, &offset );
}

//std::string utilities::convertInt(int number)
//{
//	std::stringstream ss;//create a stringstream
//	ss << number;//add number to the stream
//	return ss.str();//return a string with the contents of the stream
//}

bool SDLUtilities::fillBackground(SDL_Surface *screen) {
	//Fill the background white
	SDL_FillRect( screen, &screen->clip_rect, SDL_MapRGB( screen->format, 0xFF, 0xFF, 0xFF ) );
	return true;
}

SDL_Surface* SDLUtilities::loadImage( std::string file ) {
	SDL_Surface* loadedImage = NULL;
	SDL_Surface* optimizedImage = NULL;
	loadedImage = SDL_LoadBMP(file.c_str());
	if ( loadedImage != NULL ) {
		optimizedImage = SDL_DisplayFormat( loadedImage );
		SDL_FreeSurface( loadedImage );
	}
	return optimizedImage;
}

void SDLUtilities::freeSurface(SDL_Surface * surface) {
	SDL_FreeSurface(surface);
}

SDL_Surface * SDLUtilities::renderExistingDynamicText(SDL_Surface * oldSurface, TTF_Font * font, std::string message, SDL_Color * color) {
	//Free surface before rendering it to fix memory leak
	//Kinda sloppy, but it seems there's is no other way for dynamic text
	SDL_FreeSurface( oldSurface );
	return TTF_RenderText_Solid(font, message.c_str(), *color);
}

//void SDLUtilities::drawWorld(std::vector<std::vector<WorldLocation> > & theZone, int * startingX, int * startingY) {
//
//}
