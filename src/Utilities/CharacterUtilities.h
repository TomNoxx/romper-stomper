/*
 * CharacterUtilities.h
 *
 *  Created on: Jun 20, 2013
 *      Author: Tom
 */

#include <sstream>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "../Character/Character.h"
#include "../Character/Player/Player.h"
#include "../World/WorldLocation.h"
#include "../Singletons/Zone.h"
#include "../Singletons/MonsterList.h"
#include "../Singletons/SDLSurfaces.h"
#include "GeneralUtilities.h"
#include "CombatUtilities.h"


#ifndef CHARACTERUTILITIES_H_
#define CHARACTERUTILITIES_H_

class CharacterUtilities {
private:
	static void manageCombat(Character* c, WorldLocation* newLoc);
	static void removeCharacterForMovement(Character * c);
	static void addCharacterForMovement(Character * c);
	static void moveToNewYLoc(int newY, Character* c);
	static void moveToNewXLoc(int newX, Character* c);

public:
	static bool moveUp(Character * c);
	static bool moveDown(Character * c);
	static bool moveLeft(Character * c);
	static bool moveRight(Character * c);
	static void buildCharacterMapping(std::string mappingFile);
};

#endif /* CHARACTERUTILITIES_H_ */
