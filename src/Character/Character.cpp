/*
 * Character.cpp
 *
 *  Created on: Jun 13, 2013
 *      Author: Tom
 */

#include "Character.h"

//Character::Character(std::string i) {
//	image = i;
//}

//TODO Might not need all these image path and image string functions
void Character::setCharacterImageString(std::string imageName) {
	image = imageName;
}

void Character::setCharacterImagePath(std::string relativePath) {
	imagePath = relativePath;
}

std::string Character::getImageString() {
	return image;
}

std::string Character::getImagePath() {
	return imagePath;
}

std::string Character::getCharacterImageWithPath() {
	std::string imagePlusPath = imagePath + image;
	return imagePlusPath;
}

std::string Character::getType() {
	return type;
}

void Character::setCharacterSurface(SDL_Surface * s) {
	characterSurface = s;
}
//
//void Character::loadCharacterSurface() {
//	if (image.length() > 0) {
//		characterSurface = SDLUtilities::loadImage(getCharacterImageWithPath());
//	}
//}

bool Character::freeCharacterSurface() {
	SDL_FreeSurface(characterSurface);
	return true;
}

SDL_Surface * Character::getCharacterSurface() {
	if (characterSurface != NULL) {
		return characterSurface;
	}
	return SDLUtilities::loadImage("../../error.bmp");
}

void Character::setXLoc(int x) {
	xLoc = x;
}

int Character::getXLoc() {
	return xLoc;
}

void Character::setYLoc(int y) {
	yLoc = y;
}

int Character::getAttackBase() const {
	return attackBase;
}

void Character::setAttackBase(int attackBase) {
	this->attackBase = attackBase;
}

int Character::getHealthpoints() const {
	return healthpoints;
}

void Character::setHealthpoints(int healthpoints) {
	this->healthpoints = healthpoints;
}

int Character::getYLoc() {
	return yLoc;
}

boost::uuids::uuid Character::getUUID() {
	return uuid;
}

void Character::setUUID() {
	uuid = boost::uuids::random_generator()();
}

Character::~Character() {
	freeCharacterSurface();
}
