/*
 * Character.h
 *
 *  Created on: Jun 13, 2013
 *      Author: Tom
 */

#ifndef CHARACTER_H_
#define CHARACTER_H_

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include "SDL/SDL.h"
#include "../Utilities/SDLUtilities.h"

class Character {
protected:
	std::string image;
	std::string imagePath;
	std::string type;
	SDL_Surface *characterSurface;
	boost::uuids::uuid uuid;
	int xLoc;
	int yLoc;
	int attackBase;
	int healthpoints;
public:
	//TODO see if making these virtual also fixes the problem
//	Character(std::string i);
	virtual void setCharacterImageString(std::string imageName);
	virtual void setCharacterImagePath(std::string relativePath);
	virtual std::string getImageString();
	virtual std::string getImagePath();
	virtual std::string getCharacterImageWithPath();
	virtual std::string getType();
	virtual void setCharacterSurface(SDL_Surface * s);
//	virtual void loadCharacterSurface();
	virtual bool freeCharacterSurface();
	virtual SDL_Surface * getCharacterSurface();
	virtual void setXLoc(int x);
	virtual int getXLoc();
	virtual void setYLoc(int y);
	virtual int getYLoc();
	virtual int getAttackBase() const;
	virtual void setAttackBase(int attackBase);
	virtual int getHealthpoints() const;
	virtual void setHealthpoints(int healthpoints);
	virtual boost::uuids::uuid getUUID();
	virtual void setUUID();
	virtual ~Character();
};

#endif /* CHARACTER_H_ */
