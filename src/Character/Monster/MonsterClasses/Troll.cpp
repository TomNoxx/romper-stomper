/*
 * Troll.cpp
 *
 *  Created on: Jun 27, 2013
 *      Author: Tom
 */

#include "Troll.h"

Troll::Troll(std::string i) {
	image = i;
	setHealthpoints(50);
	setAttackBase(30);
	setUUID();
}

Monster * __stdcall Troll::Create() {
	Troll * newTroll = new Troll("Troll.bmp");
	return newTroll;
}

Troll::~Troll() {
//	freeCharacterSurface();
}

