/*
 * Troll.h
 *
 *  Created on: Jun 27, 2013
 *      Author: Tom
 */

#ifndef TROLL_H_
#define TROLL_H_

#include <string>
#include "../Monster.h"

class Troll : public Monster {
private:
public:
	Troll(std::string i);
	static Monster * __stdcall Create();
	virtual ~Troll();
};

#endif /* TROLL_H_ */
