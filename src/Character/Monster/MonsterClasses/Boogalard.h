/*
 * Boogalard.h
 *
 *  Created on: Jun 27, 2013
 *      Author: Tom
 */

#ifndef BOOGALARD_H_
#define BOOGALARD_H_

#include "../Monster.h"
#include <string>

class Boogalard : public Monster {
private:
public:
	Boogalard(std::string i);
	static Monster * __stdcall Create();
	virtual ~Boogalard();
};

#endif /* BOOGALARD_H_ */
