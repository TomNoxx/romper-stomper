/*
 * Boogalard.cpp
 *
 *  Created on: Jun 27, 2013
 *      Author: Tom
 */

#include "Boogalard.h"

Boogalard::Boogalard(std::string i) {
	image = i;
	setHealthpoints(30);
	setAttackBase(4);
	setUUID();
}

Monster * __stdcall Boogalard::Create() {
	return new Boogalard("Boogalard.bmp");
}

Boogalard::~Boogalard() {
//	freeCharacterSurface();
}

