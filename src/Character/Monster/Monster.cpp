/*
 * Monster.cpp
 *
 *  Created on: Jun 23, 2013
 *      Author: Tom
 */

#include "Monster.h"

//Monster::Monster(std::string i) : Character(i) {
//	// TODO Auto-generated constructor stub
//	type = "Monster";
//	Character::setCharacterImagePath("Images/Characters/Monsters/");
//}

boost::signals2::connection Monster::subscribeToDropItemEvents(const DropItemEventHandlerFn & fn) {
	return dropItemSignal.connect(fn);
}

void Monster::dropItem(const std::string & itemName) {
	MonsterEventManager::getInstance()->Queue( boost::bind( boost::ref(dropItemSignal), itemName));
}

void Monster::setType(std::string t) {
	type = t;
}

std::string Monster::getType() {
	return type;
}

//Challenge Rating is a TODO that will be utilized to generate stats so they do not need to be hard coded
int Monster::getChallengeRating() const {
	return challengeRating;
}

void Monster::setChallengeRating(int challengeRating) {
	this->challengeRating = challengeRating;
}

//Monster::~Monster() {
//	freeCharacterSurface();
//}

