/*
 * Monster.h
 *
 *  Created on: Jun 23, 2013
 *      Author: Tom
 */

#ifndef MONSTER_H_
#define MONSTER_H_

#include <string>
#include <boost/function.hpp>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <boost/uuid/uuid.hpp>
#include "../../EventHandlers/Managers/MonsterEventManager.h"
#include "../Character.h"


class Monster : public Character {
public:
	// Callback signature
	typedef void DropItemEventHandlerFnSignature(const std::string itemName);
	typedef boost::function<DropItemEventHandlerFnSignature> DropItemEventHandlerFn;
	boost::signals2::connection subscribeToDropItemEvents(const DropItemEventHandlerFn & fn);
	void dropItem(const std::string & itemName);
	virtual void setType(std::string t);
	virtual std::string getType();
	virtual int getChallengeRating() const;
	virtual void setChallengeRating(int challengeRating);
protected:
	//type will determine the image used
	boost::signals2::signal<DropItemEventHandlerFnSignature> dropItemSignal;
	int challengeRating;
	boost::uuids::uuid uuid;
	std::string type;
};

typedef Monster * (__stdcall *CreateMonsterFn)(void);

#endif /* MONSTER_H_ */
