/*
 * Player.cpp
 *
 *  Created on: Jun 5, 2013
 *      Author: Tom
 */

#include "Player.h"

Player::Player(std::string i) {
	image = i;
	type = "Player";
	Character::setHealthpoints(100);
	Character::setAttackBase(10);
	Character::setUUID();
}

//boost::signals2::connection Player::subscribeToUseItemEvents(const UseItemEventHandlerFn & fn) {
//	return useHealthItemSignal.connect(fn);
//}

void Player::setLevel(int level) {
	Player::level = level;
}

int Player::getLevel() {
	return level;
}

void Player::setName(std::string name) {
	Player::name = name;
}

std::string Player::getName() {
	return name;
}

void Player::setOldHP(int hp) {
	oldHealthPoints = hp;
}

int Player::getOldHP() {
	return oldHealthPoints;
}

void Player::addToInventory(IItems * item) {
	inventory.push_back(item);
}

std::vector<IItems *> * Player::getInventory() {
	return &inventory;
}

void Player::use(std::string itemName) {
	for (int i = 0; i < (int) inventory.size(); i++) {
		if (inventory[i]->getName().compare(itemName) == 0) {
			//TODO faking it now, since health is the only item.
			int hpRestored = dynamic_cast<Health*>(inventory[i])->quaff();
			int newHealth = getHealthpoints() + hpRestored;
			if (newHealth > 100) {
				newHealth = 100;
			}
			setHealthpoints(newHealth);
			inventory.erase(inventory.begin() + i, inventory.begin() + i + 1);
		} else {
			//TODO other items
		}
	}
}

bool Player::inventoryIsEmpty() {
	if ((int)inventory.size() > 0) {
		return false;
	}
	for (int i = 0; i < (int)inventory.size(); i++) {
		if(inventory.at(i) != NULL) {
			return false;
		}
	}
	return true;
}

Player::~Player() {
	freeCharacterSurface();
}
