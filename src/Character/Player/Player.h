/*
 * Player.h
 *
 *  Created on: Jun 5, 2013
 *      Author: Tom
 */

#ifndef PLAYER_H_
#define PLAYER_H_

#include "../Character.h"
#include "../../GameObjects/Items/Consumables/Health.h"
#include "../../GameObjects/Items/IItems.h"
#include "../../EventHandlers/Managers/ItemEventManager.h"
#include <boost/function.hpp>
#include <boost/signals2.hpp>
#include <boost/bind.hpp>
#include <string>
#include <vector>

class Player : public Character {
public:
	Player(std::string i);
//	typedef void UseItemEventHandlerFnSignature(Player * p, IItems * i);
//	typedef boost::function<UseItemEventHandlerFnSignature> UseItemEventHandlerFn;
//	boost::signals2::connection subscribeToUseItemEvents(const UseItemEventHandlerFn & fn);
	void setLevel(int newLevel);
	int getLevel();
	void setName(std::string name);
	std::string getName();
	void setOldHP(int hp);
	int getOldHP();
	void addToInventory(IItems * item);
	void use(std::string itemType);
	std::vector<IItems *> * getInventory();
	bool inventoryIsEmpty();
	virtual ~Player();
private:
	//TODO vector if items for inventory
	int level;
	int oldHealthPoints;
	std::string name;
	std::vector<IItems *> inventory;
//	boost::signals2::signal<UseItemEventHandlerFnSignature> useHealthItemSignal;
};

#endif /* PLAYER_H_ */
