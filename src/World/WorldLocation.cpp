/*
 * WorldLocation.cpp
 *
 *  Created on: Jun 5, 2013
 *      Author: Tom
 */

#include "WorldLocation.h"

WorldLocation::WorldLocation() {
        // TODO Auto-generated constructor stub
        hasCharacter = false;
        hasItems = false;
        traversable = true;
}

void WorldLocation::setCharacter(Character * p) {
        characterPtr = p;
        hasCharacter = true;
}

bool WorldLocation::containsCharacter() {
        return hasCharacter;
}

Character * WorldLocation::getCharacter() {
        return characterPtr;
}

void WorldLocation::removeCharacter() {
        hasCharacter = false;
}

void WorldLocation::setTileValue(std::string tV) {
        tileValue = tV;
}

std::string WorldLocation::getTileValue() {
        return tileValue;
}

void WorldLocation::setSurface(SDL_Surface * s) {
        surface = s;
}

SDL_Surface * WorldLocation::getSurface() {
//      if (characterPtr != NULL) {
//              return characterPtr->getCharSurface();
//      } else {
                return surface;
//      }

}

void WorldLocation::setTraversable(bool t) {
        traversable = t;
}

bool WorldLocation::isTraversable() {
        return traversable;
}

void WorldLocation::placeItems(std::vector<IItems*> * items) {
	for(int i = 0; i < (int) items->size(); i++) {
		//TODO this line is what is crashing the game
		IItems * placeThis = items->at(i);
		pickUpableItems.push_back(placeThis);
		hasItems = true;
	}
}

std::vector<IItems*> * WorldLocation::getItems() {
	hasItems = false;
	return &pickUpableItems;
}

bool WorldLocation::containsItems() {
	return hasItems;
}

void WorldLocation::setContainsItems(bool hi) {
	hasItems = hi;
}

WorldLocation::~WorldLocation() {
        // TODO Auto-generated destructor stub
}
