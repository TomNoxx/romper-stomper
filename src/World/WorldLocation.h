/*
 * WorldLocation.h
 *
 *  Created on: Jun 5, 2013
 *      Author: Tom
 */

#ifndef WORLDLOCATION_H_
#define WORLDLOCATION_H_

#include <string>
#include <vector>
#include "SDL.h"
#include "../Character/Character.h"
#include "../GameObjects/Items/IItems.h"

class IItems;
class Character;
class WorldLocation {
public:
        WorldLocation();
        void setCharacter(Character * p);
        bool containsCharacter();
        Character * getCharacter();
        void removeCharacter();
        void setTileValue(std::string tV);
        std::string getTileValue();
        void setSurface(SDL_Surface * s);
        SDL_Surface * getSurface();
        void setTraversable(bool t);
        bool isTraversable();
        void placeItems(std::vector<IItems*> * items);
        std::vector<IItems*> * getItems();
        bool containsItems();
        void setContainsItems(bool hi);
        virtual ~WorldLocation();
private:
        Character * characterPtr;
        std::string tileValue;
        SDL_Surface * surface;
        std::vector<IItems*> pickUpableItems;
        bool hasCharacter;
        bool traversable;
        bool hasItems;
};

#endif /* WORLDLOCATION_H_ */
