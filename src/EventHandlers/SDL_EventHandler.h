/*
 * EventHandler.h
 *	Not a real event handler.  Just a place to pass SDL events so my main doesn't get cluttered.
 *  Created on: Jun 20, 2013
 *      Author: Tom
 */

#ifndef SDLEVENTHANDLER_H_
#define SDLEVENTHANDLER_H_

#include <vector>
#include "../World/WorldLocation.h"
#include "../Character/Character.h"
#include "../Character/Player/Player.h"
#include "../Utilities/CharacterUtilities.h"
#include "SDL/SDL.h"

class SDL_EventHandler {
public:
	static void handleCharacterEvents(SDL_Event * e, Player * p);
};

#endif /* EVENTHANDLER_H_ */
