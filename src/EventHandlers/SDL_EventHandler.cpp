/*
 * EventHandler.cpp
 *
 *  Created on: Jun 20, 2013
 *      Author: Tom
 */

#include "SDL_EventHandler.h"

//TODO put these into a queue and manage that way
void SDL_EventHandler::handleCharacterEvents(SDL_Event * e, Player * p) {
	if (e->type == SDL_KEYDOWN ) {
			if ( e->key.keysym.sym == SDLK_w ) {
				CharacterUtilities::moveUp(p);
			} else if ( e->key.keysym.sym == SDLK_a ) {
				CharacterUtilities::moveLeft(p);
			} else if ( e->key.keysym.sym == SDLK_s ) {
				CharacterUtilities::moveDown(p);
			} else if ( e->key.keysym.sym == SDLK_d ) {
				CharacterUtilities::moveRight(p);
			} else if ( e->key.keysym.sym == SDLK_p ) {
				WorldLocation * currentLoc = &Zone::getInstance()->getZone()->at(p->getYLoc()).at(p->getXLoc());
				if (currentLoc->containsItems()) {
					std::vector<IItems*> * items = currentLoc->getItems();
					for (int i = 0; i < (int)items->size(); i++) {
						p->addToInventory(items->at(i));
					}
					currentLoc->setContainsItems(false);
				}
			} else if ( e->key.keysym.sym == SDLK_q ) {
				p->use("Health");
			}
		}
}
