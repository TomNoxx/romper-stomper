///*
// * ItemEventController.h
// *
// *  Created on: Jul 13, 2013
// *      Author: Tom
// */
//
//#ifndef ITEMEVENTCONTROLLER_H_
//#define ITEMEVENTCONTROLLER_H_
//
//#include <boost/uuid/uuid.hpp>
//#include <boost/shared_ptr.hpp>
//#include <boost/weak_ptr.hpp>
//#include <boost/bind.hpp>
//#include <boost/signals2.hpp>
//#include "../../Character/Player/Player.h"
//#include "../../GameObjects/Items/IItems.h"
//#include "../../GameObjects/Items/Consumables/Health.h"
//
//class Player;
//class Health;
//class ItemEventController {
//public:
//	static ItemEventController * getInstance();
//	virtual ~ItemEventController();
//private:
//	static ItemEventController * instance;
//	static bool instanceFlag;
//	ItemEventController();
//	typedef std::vector<boost::signals2::connection> ConnectionVec;
//	void handleUseHealthItemEvent(Player * p, IItems * hp);
//	void addConnection(Player * p);
//	std::map<Player *, ConnectionVec> allConnections;
////	std::set<IItems * > itemSet;
//};
//
//#endif /* ITEMEVENTCONTROLLER_H_ */
