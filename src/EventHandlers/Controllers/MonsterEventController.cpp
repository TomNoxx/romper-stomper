/*
 * MonsterEventController.cpp
 *
 *  Created on: Jul 9, 2013
 *      Author: Tom
 */

#include "MonsterEventController.h"

MonsterEventController * MonsterEventController::instance = NULL;
bool MonsterEventController::instanceFlag = false;

MonsterEventController::MonsterEventController() {
	MonsterList * ml = MonsterList::getInstance();
	std::map<boost::uuids::uuid, Monster *> * mm = ml->getMonsterMap();
	std::map<boost::uuids::uuid, Monster *>::iterator monsterItr = mm->begin();
	while (monsterItr != mm->end()) {
//		monsterSet.insert(monsterItr->second);
		// Register event callback functions with the spaceship so he can notify us.
		// Bind a pointer to the particular spaceship so we know who originated the event.
		boost::signals2::connection dropItemConnection = monsterItr->second->subscribeToDropItemEvents(
				boost::bind( &MonsterEventController::handleDropItemEvent, this, monsterItr->second, _1 ) );

		instance->allConnections[monsterItr->second].push_back(dropItemConnection);
		++monsterItr;
	}

}

MonsterEventController * MonsterEventController::getInstance() {
	if (instanceFlag) {
		return instance;
	} else {
		instance = new MonsterEventController();
		instanceFlag = true;
		return instance;
	}
}

void MonsterEventController::addConnection(Monster * m) {
//	monsterSet.insert(m);
	boost::signals2::connection dropItemConnection = m->subscribeToDropItemEvents(
			boost::bind( &MonsterEventController::handleDropItemEvent, this, m, _1 ) );

	instance->allConnections[m].push_back(dropItemConnection);
}

void MonsterEventController::handleDropItemEvent(Monster * m, const std::string & itemName) {
	//TODO give worldlocation of the monster the item
	Zone * z = Zone::getInstance();
	std::vector<IItems*> items;
	//TODO going to have to cast this at some point.  figure out casting.
	IItems * healthItem = ItemFactory::getInstance()->createItem(itemName);
	//TODO implement item container for worldLocation
	//These lines are crashing my code
	//It is not placeItems
	items.push_back(healthItem);
	z->getZone()->at(m->getYLoc()).at(m->getXLoc()).placeItems(&items);
}

MonsterEventController::~MonsterEventController() {
	// TODO Auto-generated destructor stub
}

