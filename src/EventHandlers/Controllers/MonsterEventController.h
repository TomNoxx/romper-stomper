/*
 * MonsterEventController.h
 *
 *  Created on: Jul 9, 2013
 *      Author: Tom
 */

#ifndef MONSTEREVENTCONTROLLER_H_
#define MONSTEREVENTCONTROLLER_H_

#include <boost/uuid/uuid.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/bind.hpp>
#include <boost/signals2.hpp>
#include "../../Singletons/MonsterList.h"
#include "../../GameObjects/Items/IItems.h"
#include "../../GameObjects/Items/Consumables/Health.h"
#include "../../Singletons/Zone.h"
#include "../../Factories/ItemFactory.h"

class MonsterEventController {
public:
	static MonsterEventController * getInstance();
	virtual ~MonsterEventController();
	void addConnection(Monster * m);
private:
	static MonsterEventController * instance;
	static bool instanceFlag;
	MonsterEventController();
	typedef std::vector<boost::signals2::connection> ConnectionVec;
	void handleDropItemEvent(Monster * m, const std::string & itemName);
	std::map<Monster *, ConnectionVec> allConnections;
	std::set<Monster * > monsterSet;
};

#endif /* MONSTEREVENTCONTROLLER_H_ */
