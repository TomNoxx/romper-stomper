///*
// * ItemEventManager.h
// *
// *  Created on: Jul 13, 2013
// *      Author: Tom
// */
//
//#ifndef ITEMEVENTMANAGER_H_
//#define ITEMEVENTMANAGER_H_
//
//#include <stddef.h>
//#include <vector>
//#include <boost/foreach.hpp>
//#include <boost/bind.hpp>
//#include <boost/function.hpp>
//#include <boost/thread.hpp>
//#include <boost/thread/mutex.hpp>
//
//class ItemEventManager {
//public:
//	typedef void ItemEventNotificationFnSignature();
//	typedef boost::function<ItemEventNotificationFnSignature> ItemEventNotificationFn;
//	static ItemEventManager * getInstance();
//	void Queue(const ItemEventNotificationFn & event);
//	void TriggerEvents();
//	virtual ~ItemEventManager();
//private:
//	static ItemEventManager * instance;
//	static bool instanceFlag;
//	ItemEventManager();
//	std::vector<ItemEventNotificationFn> eventVector;
//	boost::recursive_mutex notificationProtection;
//};
//
//#endif /* ITEMEVENTMANAGER_H_ */
