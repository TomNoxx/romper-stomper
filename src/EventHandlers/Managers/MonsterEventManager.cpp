/*
 * MonsterEventManager.cpp
 *
 *  Created on: Jul 9, 2013
 *      Author: Tom
 */

#include "MonsterEventManager.h"

MonsterEventManager * MonsterEventManager::instance = NULL;
bool MonsterEventManager::instanceFlag = false;

MonsterEventManager::MonsterEventManager() {
	// TODO Auto-generated constructor stub

}

MonsterEventManager * MonsterEventManager::getInstance() {
	if (instanceFlag) {
		return instance;
	} else {
		instance = new MonsterEventManager();
		instanceFlag = true;
		return instance;
	}
}

void MonsterEventManager::Queue(const MonsterEventNotificationFn & event) {
	boost::recursive_mutex::scoped_lock lock(  notificationProtection );
	eventVector.push_back(event);
}

void MonsterEventManager::TriggerEvents() {
	 std::vector<MonsterEventNotificationFn> vecNotifications;

	// Open a protected scope to modify the notification list
	{
		// One thread at a time
		boost::recursive_mutex::scoped_lock lock( notificationProtection );

		// Copy the notification vector to our local list and clear it at the same time
		std::swap( vecNotifications, eventVector );
	}

	// Now loop over the notification callbacks and call each one.
	// Since we're looping over the copy we just made, new events won't affect us.
	BOOST_FOREACH( const MonsterEventNotificationFn & fn, vecNotifications )
	{
		fn() ;
	}
}

MonsterEventManager::~MonsterEventManager() {
	// TODO Auto-generated destructor stub
}

