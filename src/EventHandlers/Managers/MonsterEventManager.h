/*
 * MonsterEventManager.h
 *	This is heavily influenced by an example found on stackoverflow
 *	http://stackoverflow.com/questions/7464025/designing-an-event-mechanism-in-c
 *  Created on: Jul 9, 2013
 *      Author: Tom
 */

#ifndef MONSTEREVENTMANAGER_H_
#define MONSTEREVENTMANAGER_H_

#include <stddef.h>
#include <vector>
#include <boost/foreach.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>

class MonsterEventManager {
public:
	typedef void MonsterEventNotificationFnSignature();
	typedef boost::function<MonsterEventNotificationFnSignature> MonsterEventNotificationFn;
	static MonsterEventManager * getInstance();
	void Queue(const MonsterEventNotificationFn & event);
	void TriggerEvents();
	virtual ~MonsterEventManager();
private:
	static MonsterEventManager * instance;
	static bool instanceFlag;
	//I guess private needs to be below for EventNotificationFn to evaluate here
	std::vector<MonsterEventNotificationFn> eventVector;
	boost::recursive_mutex notificationProtection;
	MonsterEventManager();
};

#endif /* MONSTEREVENTMANAGER_H_ */
