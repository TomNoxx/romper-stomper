///*
// * ItemEventManager.cpp
// *
// *  Created on: Jul 13, 2013
// *      Author: Tom
// */
//
//#include "ItemEventManager.h"
//
//ItemEventManager * ItemEventManager::instance = NULL;
//bool ItemEventManager::instanceFlag = false;
//
//ItemEventManager::ItemEventManager() {
//	// TODO Auto-generated constructor stub
//
//}
//
//ItemEventManager * ItemEventManager::getInstance() {
//	if (instanceFlag) {
//		return instance;
//	} else {
//		instance = new ItemEventManager();
//		instanceFlag = true;
//		return instance;
//	}
//}
//
//void ItemEventManager::Queue(const ItemEventNotificationFn & event) {
//	boost::recursive_mutex::scoped_lock lock(  notificationProtection );
//	eventVector.push_back(event);
//}
//
//void ItemEventManager::TriggerEvents() {
//	 std::vector<ItemEventNotificationFn> vecNotifications;
//
//	// Open a protected scope to modify the notification list
//	{
//		// One thread at a time
//		boost::recursive_mutex::scoped_lock lock( notificationProtection );
//
//		// Copy the notification vector to our local list and clear it at the same time
//		std::swap( vecNotifications, eventVector );
//	}
//
//	// Now loop over the notification callbacks and call each one.
//	// Since we're looping over the copy we just made, new events won't affect us.
//	BOOST_FOREACH( const ItemEventNotificationFn & fn, vecNotifications )
//	{
//		fn() ;
//	}
//}
//
//ItemEventManager::~ItemEventManager() {
//	// TODO Auto-generated destructor stub
//}
//
