/******************************************************
 * General Notes
 * 	-when access the player, always use a player pointer
 *
 */

/*******************************************************
 * Overall TODOs
 * TODO Create a larger zone and implement bounds when drawing the zone
 * TODO create a gameover aspect
 * TODO make it so monsters cannot move into the same square so they do not overlap and delete one another
 * 		-probably a threading issue.  lock on the worldlocation most likely in order
 * TODO long and tedious, but rename things to have a convention
 * 		-ie	ICharacter for the character interface
 * 		-	pSurface for an SDL_Surface Pointer
 * 		-look up C++ standards
 * TODO create a new function for player movement that can handle the various checkpoints no matter what direction
 * TODO look at character utilities TODO for manageCombat
 * TODO inventory interface
 * TODO solution to current circular inclusion between IItems and Player.
 * 		-My last attempt ignored my original plan.  that's what went wrong.
 * 		-Create player event manager/controller
 * 		-an event will be consuming a health potion
 * 		-when the player uses the potion, use will kick of a consume health potion, passing a int value to the event.
 * 		-the event will restore health to the player
 * TODO create a monster thread singleton to add new threads when monsters are created
 * TODO create monster needs to create a thread
 * TODO create a monster spawner for runtime creation - thread
 * TODO make it so the worldLocation does not need to hold a character pointer (potentially)
 * 		--event handling / event listeners?
 * ----LOWER PRIORITY----
 * TODO create a ?Template? for monsters so a user can create monsters via txt file config, rather than code
 * 		-also, move .txt files outside of compiles project so they're accessable? (I'm used to java an all these being in a .jar)
 * TODO intelligently load the zone to reduce processing
 * TODO Monsters scan for the player
 */

//libraries and stuff
#include "SDL.h"
#include "SDL_ttf.h"
#include <boost/thread.hpp>
#include <boost/random.hpp>
#include <boost/progress.hpp>
#include <string>
#include <map>

//my code
#include "src/Character/Character.h"
#include "src/Character/Player/Player.h"
#include "src/Character/Monster/Monster.h"
#include "src/Character/Monster/MonsterClasses/Boogalard.h"
#include "src/Character/Monster/MonsterClasses/Troll.h"
#include "src/Utilities/GeneralUtilities.h"
#include "src/Utilities/SDLUtilities.h"
#include "src/Utilities/PlayerUtilities.h"
#include "src/Utilities/WorldUtilities.h"
#include "src/EventHandlers/SDL_EventHandler.h"
#include "src/EventHandlers/Managers/MonsterEventManager.h"
#include "src/EventHandlers/Managers/ItemEventManager.h"
#include "src/EventHandlers/Controllers/MonsterEventController.h"
#include "src/EventHandlers/Controllers/ItemEventController.h"
#include "src/World/WorldLocation.h"
#include "src/Singletons/MonsterSingleton.h"
#include "src/Singletons/Zone.h"
#include "src/Singletons/MonsterList.h"
#include "src/Factories/MonsterFactory.h"
#include "src/ThreadClasses/MonsterActionThread.h"

//The surfaces that will be used
SDL_Surface *playerHealthMessageSurface = NULL;
SDL_Surface *background = NULL;
SDL_Surface *screen = NULL;
TTF_Font *playerHealth = NULL;
SDL_Color red = { 255, 0, 0  };
SDL_Color yellow = { 255, 255, 0 };
SDL_Color green = { 0, 255, 0 };
SDL_Color healthColor;
bool quit = false;
SDL_Event event;

int tileSize;

std::string const TILE_MAPPING_FILE = "src/World/Tiles/TileMapping.txt";
std::string const STARTING_ZONE_FILE = "src/World/Zones/StartingZone.txt";
std::string const CHARACTER_MAPPING_FILE = "src/Character/CharacterImageList.txt";

std::string GAME_TITLE = "Romper Stomper: Tales of the Half-Orc Barbarian";

bool load_fonts() {
	playerHealth = TTF_OpenFont( "DigitalDream.ttf", 16 );

	if (playerHealth == NULL) {
		return false;
	}

	return true;
}

bool init() {
	 //Initialize all SDL subsystems
	if( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
	{
		return false;
	}

	if ( TTF_Init() == -1 ) {
		return false;
	}

	if (!load_fonts()) {
		return false;
	}

	//Set up the screen
	screen = SDL_SetVideoMode( Zone::getInstance()->getScreenWidth(), Zone::getInstance()->getScreenHeight(), Zone::getInstance()->getScreenBpp(), SDL_SWSURFACE );

	//If there was an error in setting up the screen
	if( screen == NULL )
	{
		return false;
	}
	return true;
}

int main( int argc, char* args[] )
{
	//TODO create init in SDLUtilities
	if (!init()) {
		return 6;
	}

	boost::progress_timer loadTimer;
	//create singleton pointers for main
	Zone * zone = Zone::getInstance();
	MonsterSingleton * ms = MonsterSingleton::getInstance();
	MonsterList * ml = MonsterList::getInstance();
	SDLSurfaces * sdls = SDLSurfaces::getInstance();
	MonsterEventManager * mem = MonsterEventManager::getInstance();
	MonsterEventController * mec = MonsterEventController::getInstance();
//	ItemEventController * iec = ItemEventController::getInstance();

	tileSize = 50;
	std::map<std::string, std::string> tileMapping;

	WorldUtilities::setTileValueMapping(tileMapping, TILE_MAPPING_FILE);

	CharacterUtilities::buildCharacterMapping(CHARACTER_MAPPING_FILE);

	WorldUtilities::setZoneValuesToWorld(tileMapping, STARTING_ZONE_FILE);

	zone->setScreenXLowerBound(0);
	int xUB = (zone->getScreenWidth() / tileSize);
	zone->setScreenXUpperBound(xUB);
	zone->setScreenYLowerBound(0);
	int yUB = (zone->getScreenHeight() / tileSize);
	zone->setScreenYUpperBound(yUB);

	std::string playerString = "Romper Stomper Smash";
	Player player = Player("Player.bmp");
	Player * playerPtr= &player;
	playerPtr->setName("Noxxia");
	//simple thread test
	playerPtr->setLevel(1);
	playerPtr->setXLoc(5);
	playerPtr->setYLoc(2);

	zone->getZone()->at(playerPtr->getYLoc()).at(playerPtr->getXLoc()).setCharacter(playerPtr);

	std::string troll = "Troll";
	std::string boogalard = "Boogalard";

	boost::thread_group monsterActionThreads;

	//TODO This until Marked "END" belongs in a monster spawner
	Monster * blard = MonsterFactory::getInstance()->createMonster(boogalard);
	Monster * tr = MonsterFactory::getInstance()->createMonster(troll);

	ml->insertMonster(blard->getUUID(), blard);
	ml->insertMonster(tr->getUUID(), tr);
	//END

    //Set Up monster threads
	std::map<boost::uuids::uuid, Monster *>::iterator mlMonsterMapItr = ml->getMonsterMap()->begin();
    while (mlMonsterMapItr != ml->getMonsterMap()->end()) {
    	MonsterActionThread mat(mlMonsterMapItr->second);
    	boost::thread monsterThread(mat);
    	monsterActionThreads.add_thread(&monsterThread);
    	++mlMonsterMapItr;
    }

    //Load the zone images into the zone singleton map
    if (!WorldUtilities::loadZoneImages(&tileMapping)) {
		return 5;
	}

	playerPtr->setCharacterSurface(sdls->getFromCharacterImageMap(playerPtr->getType()));
	mlMonsterMapItr = ml->getMonsterMap()->begin();
	while (mlMonsterMapItr != ml->getMonsterMap()->end()) {
		//use the "type" on the monster to set the SDL_Surface * from the character mapping in SDLSurfaces
		mlMonsterMapItr->second->setCharacterSurface(sdls->getFromCharacterImageMap(mlMonsterMapItr->second->getType()));
		++mlMonsterMapItr;
	}

	//load time test
	//double elapsedTime = loadTimer.elapsed();

	//Set the window caption
	SDL_WM_SetCaption( GAME_TITLE.c_str(), NULL );
	//load time test
	//SDL_WM_SetCaption( GeneralUtilities::convertDouble(elapsedTime).c_str(), NULL );

    while ( quit == false ) {
		while (SDL_PollEvent ( &event ) ) {

			SDL_EventHandler::handleCharacterEvents(&event, playerPtr);

			if (event.type == SDL_QUIT ) {
				ms->setQuit(true);
				quit = true;
			}
		}

		//TODO move this to a utilities class
		//This should prevent the message surface from needed to be freed and rerendered every time the screen is drawn
		int playerHP = playerPtr->getHealthpoints();
		if (playerHP != playerPtr->getOldHP()) {
			if (playerHP >= 80) {
				healthColor = green;
			} else if (playerHP >= 50) {
				healthColor = yellow;
			} else {
				healthColor = red;
			}
			playerPtr->setOldHP(playerHP);
			std::string healthText = GeneralUtilities::convertInt(playerHP).c_str();
			playerHealthMessageSurface = SDLUtilities::renderExistingDynamicText(playerHealthMessageSurface, playerHealth, healthText, &healthColor);
		}

		//TEST PASSED
//		std::vector<IItems *> * items = playerPtr->getInventory();
//		if (!playerPtr->inventoryIsEmpty()) {
//			playerPtr->setCharacterSurface(SDLUtilities::loadImage("error.bmp"));
//		} else {
//			playerPtr->setCharacterSurface(SDLUtilities::loadImage("Images/Characters/Player/Player.bmp"));
//		}

		//TODO after move is completed, figure out how to make an event that will make this occur only
		//when the screen should be reloaded to save processing
		//could trigger a boolean in zone singleton on player event or monster move
		if (!WorldUtilities::setZoneImages()) {
			return 5;
		}

		if(!SDLUtilities::fillBackground(screen)) {
			return 2;
		}

		//Load all images before flipping.
		if (!WorldUtilities::drawZone(screen)) {
			return 3;
		}

		SDLUtilities::apply_surface(10, 10, playerHealthMessageSurface, screen);

		//Update the screen
		if( SDL_Flip( screen ) == -1 )
		{
			return 4;
		}

		//TODO this causes the game to crash
		mem->TriggerEvents();
    }

    monsterActionThreads.join_all();

    //Free the surfaces
    SDL_FreeSurface( playerHealthMessageSurface );
    SDL_FreeSurface( background );

    if (playerPtr->freeCharacterSurface()) {
    	return 8;
    }
    mlMonsterMapItr = ml->getMonsterMap()->begin();
    while (mlMonsterMapItr != ml->getMonsterMap()->end()) {
    	if (mlMonsterMapItr->second->freeCharacterSurface()) {
    		return 8;
    	}
    	++mlMonsterMapItr;
    }

    if (!WorldUtilities::freeZoneSurfaces()) {
    	return 9;
    }
//
//    delete ms;
//    delete ml;
//    delete world;

    //Quit SDL
    SDL_Quit();

    return 0;
}
